# defaults, render, process_inputs, calc
SPOTLIGHT_SPEED = 5
CRIMINAL_SPEED = 5

def tick args
  args.state.start_tick ||= 0
  args.state.game_state ||= :play
  args.state.spotlight_centre ||= {
    x: 500,
    y: 500
  }
  args.state.criminal_location ||= {
    x: rand(1000) + 100,
    y: rand(500) + 100
  }

  args.state.criminal_target ||= (calc_criminal_target args)
 
  spotlight_dx = 0
  spotlight_dy = 0

  args.outputs.sprites << {
    x: 0,
    y: 0,
    w: 1280,
    h: 720,
    path: "sprites/brickwall.png"
  }
  args.outputs.sprites << (calc_criminal_renderable args)
  args.outputs.sprites << (calc_spotlight_renderable args)

  keyboard = args.inputs.keyboard
  if args.state.game_state == :play
    if keyboard.key_held.up
      spotlight_dy += SPOTLIGHT_SPEED
    end
    if keyboard.key_held.down
      spotlight_dy -= SPOTLIGHT_SPEED
    end
    if keyboard.key_held.right
      spotlight_dx += SPOTLIGHT_SPEED
    end
    if keyboard.key_held.left
      spotlight_dx -= SPOTLIGHT_SPEED
    end

    potential_x = args.state.spotlight_centre.x + spotlight_dx
    potential_y = args.state.spotlight_centre.y + spotlight_dy

    if potential_x >= 0 and potential_x <= 1280
      args.state.spotlight_centre.x = potential_x
    end

    if potential_y >= 0 and potential_y <= 720
      args.state.spotlight_centre.y = potential_y
    end

    move_criminal args

    seconds_left = 20 - ((args.state.tick_count / 60) - (args.state.start_tick / 60))
    if seconds_left <= 0
      args.state.game_state = :ended
    end

    args.outputs.labels << [{
      x: args.grid.left.shift_right(25),
      y: args.grid.top.shift_down(25),
      text: "Criminal is escaping in #{seconds_left.to_i + 1} seconds!",
      size_enum: 5,
      r: 255,
      g: 255,
      b: 255
    }, {
      x: args.grid.right.shift_left(450),
      y: args.grid.top.shift_down(25),
      text: "Keep the light on him so he can't escape!",
      r: 255,
      g: 255,
      b: 255
    }]
  elsif args.state.game_state == :ended
    if (is_criminal_spotted args)
      message = "You got him! Nice work"
    else
      message = "Damn, he got away!"
    end

    args.outputs.labels << {
      x: 650,
      y: 650,
      text: message,
      size_enum: 25,
      r: 255,
      g: 255,
      b: 255,
      alignment_enum: 1
    }

    args.outputs.labels << {
      x: 650,
      y: 575,
      text: "Press <space> to play again",
      size_enum: 15,
      r: 255,
      g: 255,
      b: 255,
      alignment_enum: 1
    }

    if keyboard.key_held.space
      args.state.game_state = :play
      args.state.start_tick = args.state.tick_count
      args.state.criminal_target = (calc_criminal_target args)
    end
  end
end

def calc_spotlight_renderable args
  {
    x: args.state.spotlight_centre.x,
    y: args.state.spotlight_centre.y,
    w: 200,
    h: 200,
    path: "sprites/circle-200.png",
    r: 245,
    g: 243,
    b: 62,
    a: 100
  }.anchor_rect(-0.5, -0.5)
end

def calc_criminal_renderable args
  {
    x: args.state.criminal_location.x,
    y: args.state.criminal_location.y,
    w: 50,
    h: 82,
    path: "sprites/bigcriminal.png",
    flip_horizontally: args.state.tick_count.mod(20) < 10
  }.anchor_rect(-0.5, -0.5)
end

def calc_criminal_target args
  # create new target
  new_x = rand(1200) + 40
  new_y = rand(600) + 40

  # work out direct vector to target

  # calculate difference to travel in each axis each movement
  diff_x = (args.state.criminal_location.x - new_x).abs
  diff_y = (args.state.criminal_location.y - new_y).abs
  sum_diff = diff_x + diff_y
  
  dx = (diff_x * CRIMINAL_SPEED)/(sum_diff)
  dy = (diff_y * CRIMINAL_SPEED)/(sum_diff)

  # set dx and dy to negative if the criminal needs to move left/down to get to target
  if args.state.criminal_location.x > new_x
    dx *= -1
  end
  if args.state.criminal_location.y > new_y
    dy *= -1
  end

  args.state.criminal_vector = {
    dx: dx,
    dy: dy
  }

  {
    x: new_x,
    y: new_y
  }
end

def is_criminal_spotted args
  diff_x = (args.state.spotlight_centre.x - args.state.criminal_location.x).abs
  diff_y = (args.state.spotlight_centre.y - args.state.criminal_location.y).abs

  total_dist = Math.sqrt(diff_x * diff_x + diff_y * diff_y)

  return total_dist <= 130
end

def move_criminal args
  diff_x = (args.state.criminal_location.x - args.state.criminal_target.x).abs
  diff_y = (args.state.criminal_location.y - args.state.criminal_target.y).abs

  # if the criminal is at their target, choose a new target
  if diff_x < 5 && diff_y < 5
    args.state.criminal_target = calc_criminal_target args
  end

  args.state.criminal_location.x += args.state.criminal_vector.dx
  args.state.criminal_location.y += args.state.criminal_vector.dy
end